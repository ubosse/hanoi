#pragma once
#ifndef HANOIFELD_H
#define HANOIFELD_H
#include <QMainWindow>
#include "scheibe.h"
#include "handarbeiter.h"
#include "solver.h"
class Handarbeiter;
namespace Ui {
class Hanoifeld;
}

typedef QList<Zug> Zugliste;
class Hanoifeld : public QMainWindow
{
    Q_OBJECT
public:
    typedef enum {normal,solverRunning,solverPaused} State;
    State state=normal;
    static Hanoifeld*spielfeld;//zeigt auf das Spielfeld. Davon gibts ja nur eins.
    explicit Hanoifeld(QWidget *parent = 0);
    short getHoehe()const{return hoehe;}//Gesamtzahl der Scheiben.
    int baseHoehe=170;//Höhe der Unterkante der untersten Scheibe auf den Stangen
    int stangenPos[4]={115,315,515,715};
    void clearStangen(){for(short i=0;i<4;i++)stangen[i].scheiben.clear();}
    Stange*getStange(short nr){if(nr<0||nr>3) return nullptr; else return &stangen[nr];}
    Stange*getStangeOf(Scheibe*scheibe, short *pos=0);//liefert die Stange der Scheibe und deren Position darauf
    short getStangenIndexOf(Scheibe*scheibe, short *pos=0);//liefert index der Stange der Scheibe
    short getStangenIndexOf(short nr,short*pos=0){return getStangenIndexOf(scheiben.at(nr),pos);}
    QSequentialAnimationGroup *geheZu(int nr, short nachstange);//Scheibe nr wird vonstange nachstange animiert versetzt.
    QPropertyAnimation*liftScheibe(short nr, int duration=500);
    QPropertyAnimation*shiftScheibe(short nr, short nachstange, int duration=500);//verschiebt die Scheibe nr über die nachstange, hierbei wechselt die Scheibe die Stange.
    QPropertyAnimation*dropScheibe(short nr, int duration=200);//lässt die Scheibe auf die Stange fallen, über der sie ist.
    QList<Zug> *createZugliste();
    void setMovingScheibe(short s){movingScheibe=s;}
    QToolBar*toolBar=nullptr;
    bool turbo=false;
    QAction* showThoughts;//zeigt an, ob der Solver seine Gedanken zeigen soll.
    QAction* ssM;//single-step-mode für den thinker
    Gedankenzeiger thinker;//protokolliert die Überlegungen des Solvers.

private:
    Ui::Hanoifeld *ui;
    int movingScheibe=-1;//zeigt an, welche Scheibe durch geheZu gerade bewegt wird.
    QSpinBox*hoehenSpinner=nullptr;
    Stange stangen[4];
    short hoehe=0;//Höhe des Hanoiturms=Gesamtzahl aller Scheiben
    Scheibenliste scheiben;
    QWidget*base;
    QLabel*zaehler;//zur Anzeige der Zugzahl in der Statusleiste
    QAction*solveAction=nullptr;
    Handarbeiter*manual=nullptr;
    static void loese(short n,short von,short nach,Zugliste&liste);
    virtual void keyPressEvent(QKeyEvent *event);
    void SlotStopSolving(){emit stopSolving();}
private slots:
    void testSlot();
    void generalSolve();
    void showNumbers(bool on);
    void reset();//fragt in Popup nach Zielstange.
    void setHoehe(int h);
    void setTurbo(bool on){turbo=on;setFocus();}

signals:
    void resetted();
    void stopSolving();
friend class Handarbeiter;
friend class Solver;
};
class Animateur:public QObject
{
    Q_OBJECT
public:
    Animateur(Zugliste*liste, Hanoifeld *hanoi);
public slots:
    void stop();
    void pause();
private slots:
    void nextMove();
private:
    Zugliste*list;
    Hanoifeld*feld;
    QAction*pausenAction=nullptr;
    QSequentialAnimationGroup*animation=0;
    QToolBar*toolbar=nullptr;
};
class Resetter:public QObject
{
    Q_OBJECT
public:
    Resetter(Hanoifeld*hanoi,short dahin=0);//setzt animiert alle Scheiben auf Stapel dahin. Bei dahin=4
      //werden sie zufällig verteilt.
private:
    Hanoifeld*feld;
    QList<short> scheibe;//Folge der scheibennummern, die versetzt werden
    QList<short> ziel;//speichert die Zielstangen ziel[scheibe[i]]=Zielstange für Scheibe scheibe[i];
    short i=0;//gibt an, welche Scheibe als nächstes gesetzt wird, nämlich scheibe[i].
private slots:
    void next();

};
#endif // HANOIFELD_H
