#pragma once
#ifndef SOLVER_H
#define SOLVER_H
//#include "hanoifeld.h"
#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <QObject>
class Hanoifeld;
class Zug{
public:
    short nr,von,nach;
    Zug(){}
    Zug(short snr,short source,short ziel){nr=snr;von=source;nach=ziel;}
};
class Solver : public QObject
{
    Q_OBJECT
public:
    explicit Solver(Hanoifeld*parent , short snr, short zstange);
    ~Solver();


private:
    Hanoifeld * hanoi;
    short scheibennr;
    short zielstange;
    int count=0;
    QAction*pausenAction=nullptr;
    QSequentialAnimationGroup*animation=0;
    QToolBar*toolbar=nullptr;
    QSequentialAnimationGroup*nextMove(short snr, short zst);//liefert den nächsten Zug zur Lösung
public slots:
    void stop();
    void pause();
    void next();//wird mit dem finished-Slot der Animation verbunden und soll die nächste starten.
};

class Gedankenzeiger : public QList<Zug> /* Liste von Zügen und eine AnimationGroup, die gestartet wird sobald der letzte Zug der Liste
                                            sichtbar gemacht wurde. Die Einstellungen der Animationgroup sind vor dem Start des
                                            Gedankenzeigers zu treffen */
{

public:
    QAnimationGroup*aG;
    int delay=1000;
    bool singleStepMode=false;
    bool running=false;
    void setAnimationGroup(QAnimationGroup*move);
    void start(int del=1000);//startet die Anzeige der Gedanken. Jeder Zug wird delay ms angezeigt.
    void next();//zeigt den ersten Zug der Liste und entfernt ihn von der Liste. Setzt timer auf sich selbst. Falls nichts mehr zu zeigen
                //ist: die aG starten.
    void reset();

};

#endif // SOLVER_H
