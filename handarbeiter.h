#pragma once
#ifndef HANDARBEITER_H
#define HANDARBEITER_H
#include <QtCore>
#include <QObject>
#include "hanoifeld.h"
class Hanoifeld;
class Handarbeiter : public QObject
{
public:
    class ResetValues{
    public:
        QPoint pos;
        short stange;
    };
   enum State {start,oben};
    Q_OBJECT
public:
    explicit Handarbeiter(Hanoifeld * h=nullptr);
    Hanoifeld * hanoi=nullptr;
private:
    State state=start;
    bool turmumsetzen=false;//gibt an, dass mit einer Scheibe auch alle kleineren darauf versetzt werden sollen.
    ResetValues reset;
    Scheibe*scheibe=nullptr;
    short scheibennr=0;


signals:

public slots:
    virtual void KeyboardHandler(QKeyEvent *e);
};

#endif // HANDARBEITER_H
