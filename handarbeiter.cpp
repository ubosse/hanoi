#include "handarbeiter.h"
#include "hanoifeld.h"

Handarbeiter::Handarbeiter(Hanoifeld *h):QObject(h)
{
    hanoi=h;
}

void Handarbeiter::KeyboardHandler(QKeyEvent *e)
{
    //die Pfeiltasten müssen noch abgefangen werden.
    if(hanoi->state==Hanoifeld::solverRunning){
        e->ignore();
        return;
    }
    hanoi->SlotStopSolving();
    switch(e->key()){
      case Qt::Key_Escape:
        if(state!=oben)
            return;

        scheibe->move(reset.pos);
        hanoi->getStangeOf(scheibe)->scheiben.removeAll(scheibe);
        hanoi->getStange(reset.stange)->scheiben.prepend(scheibe);
        state=start;
        break;
    case Qt::Key_Left:
      if(state!=oben)
        return;
      hanoi->shiftScheibe(scheibennr,(hanoi->getStangenIndexOf(scheibe)+3) %4,200)
              ->start(QAbstractAnimation::DeleteWhenStopped);
      break;
    case Qt::Key_Right:
        if(state!=oben)
          return;
        hanoi->shiftScheibe(scheibennr,(hanoi->getStangenIndexOf(scheibe)+1) %4,200)
             ->start(QAbstractAnimation::DeleteWhenStopped);
        break;
    case Qt::Key_Down:
    case Qt::Key_Space:
      if(state!=oben) return;
      state=start;
      hanoi->dropScheibe(scheibennr,100)
           ->start(QAbstractAnimation::DeleteWhenStopped);
      if(turmumsetzen){
          for(short s=scheibennr-1;s>=0;s--){
            hanoi->geheZu(s,hanoi->getStangenIndexOf(scheibe))->start(QAbstractAnimation::DeleteWhenStopped);
          }
          turmumsetzen=false;
      }
      break;
    case Qt::Key_T:
        turmumsetzen=true;
        break;
    }
    if(e->key()<Qt::Key_0 || e->key() > Qt::Key_9)
        return;
    short nr=e->key()-Qt::Key_1;//Achtung: nr=0 für Scheibe Key_1 !
    if(e->key()==Qt::Key_0) nr=9;
    if (nr>=hanoi->getHoehe() || (nr>3 && state==oben)) return;
    if(state!=oben){
        scheibennr=nr;
        scheibe=hanoi->scheiben.at(nr);
        reset.pos=scheibe->pos();
        reset.stange=hanoi->getStangenIndexOf(nr);
        hanoi->liftScheibe(nr,200)->start(QAbstractAnimation::DeleteWhenStopped);
        state=oben;
    }else{//state==oben, Stange gemeint
        short st=e->key()-Qt::Key_1;
        hanoi->shiftScheibe(scheibennr,st,100)->start(QAbstractAnimation::DeleteWhenStopped);
    }
}
