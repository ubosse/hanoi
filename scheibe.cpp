#include "scheibe.h"
#define BREITE 15
#include <QtWidgets>
#include <QtCore>
short Scheibe::hoehe=11;
bool Scheibe::showNumbers=false;
short Scheibe::highlighted=-1;
short Stange::highlighted=-1;
Scheibe::Scheibe(short asize, QWidget *parent) : QWidget(parent)
{
    size=asize;
    setGeometry(QRect(0,0,BREITE*size+25,hoehe));
}

void Scheibe::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    QColor farbe(size==highlighted?"#D03030":"#592601");
    p.setPen(farbe);
    p.setBrush(farbe.lighter(size%2==0?150:200));
    p.drawRoundedRect(rect(),4.0,4.0);
    if(showNumbers){
        p.setPen(Qt::white);
        p.drawText(rect(),Qt::AlignLeft|Qt::AlignVCenter,QString::number(size+1));
    }
    event->accept();
}

Stange::Stange(short h, QWidget *parent):QWidget(parent)
{
    setGeometry(QRect(0,0,BREITE/2,h));
    hoehe=h;
}

void Stange::setHoehe(short h)
{
    hoehe=h;
    setGeometry(QRect(0,0,BREITE/2,hoehe));
}

void Stange::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    QColor farbe(pos==highlighted?"#E07070":"#301300");
    p.setPen(farbe);
    p.setBrush(farbe.lighter());
    p.drawRoundedRect(rect(),4.0,4.0);
    event->accept();
}
