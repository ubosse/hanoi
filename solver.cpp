#include "solver.h"
#include "scheibe.h"
#include "hanoifeld.h"
#include <QtCore>
Solver::Solver(Hanoifeld *parent, short snr, short zstange) : QObject(parent)
{
    hanoi=parent;
    scheibennr=snr; zielstange=zstange;
    connect(hanoi,SIGNAL(resetted()),this,SLOT(stop()));
    toolbar=new QToolBar("Animations-Steuerung");
    pausenAction=toolbar->addAction(QIcon(":/images/media-playback-pause.png"),"Pause",this,SLOT(pause()));
    pausenAction->setShortcut(Qt::Key_Space);
    QAction*act=toolbar->addAction(QIcon(":/images/media-playback-stop.png"),"Stop",this,SLOT(stop()));
    act->setShortcut(Qt::Key_S);
    hanoi->addToolBar(Qt::RightToolBarArea,toolbar);
    hanoi->solveAction->setDisabled(true);
    hanoi->state=Hanoifeld::solverRunning;
    connect(hanoi,SIGNAL(stopSolving()),this,SLOT(stop()));
}

Solver::~Solver()
{
    hanoi->solveAction->setEnabled(true);
    hanoi->state=Hanoifeld::normal;
}

QSequentialAnimationGroup *Solver::nextMove(short snr,short zst)
{
    if(Hanoifeld::spielfeld->showThoughts){
        Hanoifeld::spielfeld->thinker.append(Zug(snr,-1,zst));
    }
    short pos;
    short sourcestange=hanoi->getStangenIndexOf(snr,&pos);//pos ist nun die Position der snr auf der sourcestange
    Scheibe*scheibe=hanoi->scheiben.at(snr);
    //checken, ob Turbo möglich
    if(hanoi->turbo){
        bool moglich=false;
        if(pos>0 && sourcestange!=zst && pos==scheibe->getSize()){
            moglich=true;
            Stange*s=hanoi->getStange(sourcestange);
            for(short i=0;i<pos;i++){
                if(s->scheiben.at(i)->getSize()!=i){
                    moglich=false;
                    break;
                }
            }
        }
        if(moglich){
            for (short i=snr;i>0;i--){
                QSequentialAnimationGroup*aG=hanoi->geheZu(i,zst);
                aG->start(QAbstractAnimation::DeleteWhenStopped);
            }
            count+=(1 << (snr+1))-1;
            hanoi->zaehler->setText(QString::fromUtf8("Züge: %1").arg(count));
            return hanoi->geheZu(0,zst);
        }
    }
    short fehlstand[3];
    QList<Scheibe*> zugaenglich;
    for (short i =0;i<3;i++){//zugängliche Scheiben auf Stange i ermitteln,
                //fehlstand[i]=Scheibennummer des Fehlstandes (falls existent) auf
                //Stange i, sonst hoehe+1, bzw. bei der Zielstange: hoehe+2
        fehlstand[i]=hanoi->hoehe+1;
        if(i==zst) fehlstand[i]+=1;
        short fst=0;bool found=false;
        for(Scheibe*s:hanoi->getStange(i)->scheiben){
            if(s->getSize()<fst){
                found=true;//fehlstand auf Stange i gefunden! Scheibe fst ist dieser.
                break;
            }
            fst=s->getSize();
            //die Scheibe s ist eine zugänglicheScheibe und muss in die Liste an richtiger Stelle einsortiert werden
            int k=0;
            for(k=0;k<zugaenglich.count();k++){//richtige Stelle finden
              if(zugaenglich.at(k)->getSize()>fst)
                  break;
            }
            zugaenglich.insert(k,s);//einsortieren.
        }
        if(found){//FEhlstand notieren, falls einer da.
            fehlstand[i]=fst;
        }
    }//Alle Stangen nach Fehlständen fertig untersucht und Liste zugänglicher Scheiben fertig erstellt.
    short kleinsterFehlstand=hanoi->hoehe+2;
    short groessterFehlstand=-1;
    short stgrfst;
    //short stklfst;
    for(short i=0;i<3;i++){//kleinsten und größten Fehlstand ermitteln und seine Stange;
        if(fehlstand[i]<kleinsterFehlstand){
            kleinsterFehlstand=fehlstand[i];
            //stklfst=i;
        }
        if(fehlstand[i]>groessterFehlstand){
            groessterFehlstand=fehlstand[i]; stgrfst=i;
        }
    }
    if(snr>kleinsterFehlstand ||(snr==kleinsterFehlstand && zst!=stgrfst)){//erst Fehlstand bereinigen!
        return nextMove(kleinsterFehlstand,stgrfst);
    }
    //ab hier ist die Scheibe snr zugänglich
    //testen ob Scheibe direkt versetzt werden kann: sie ist zuoberst und zielstgange ist "frei"
    //die zu versetzende Scheibe ist auf der Stange sourcestange an der Position pos.
    if(pos==0){//Scheibe ist als oberste Scheibe ihres Stapels direkt zugänglich
        if(zst!=sourcestange){//zielstange verschieden von Sourcestagne
            if(hanoi->getStange(zst)->scheiben.isEmpty() ||
                    hanoi->getStange(zst)->scheiben.at(0)->getSize()>scheibe->getSize()){//Scheibe snr kann versetzt werden
                count++;
                hanoi->zaehler->setText(QString::fromUtf8("Züge %1").arg(count));
/************************* Hier jetzt die Stelle, wo die zu bewegende Scheibe ermittelt wurde und bewegt wird *******************/
                return hanoi->geheZu(snr,zst);
            }
            //zielstange ist nicht frei.  Nächstkleinere Scheibe muss versetzt werden.
            short neuesZiel=3-zst-sourcestange;
            short k=zugaenglich.indexOf(scheibe);
            if(k<=0){
                QMessageBox::warning(0,"Riesenkacke","scheissfehler in nextMove");
                return 0;
            }
            return nextMove(zugaenglich.at(k-1)->getSize(),neuesZiel);
        }
        if(zugaenglich.indexOf(scheibe)==0){
            qDebug()<<"kleinste Scheibe bereits dort wo sie sein soll: Fehler?";
            return 0;
        }
        return nextMove(zugaenglich.at(zugaenglich.indexOf(scheibe)-1)->getSize(),zst);//wenn die snr bereits auf zst ist.
    }
    //Scheibe ist nicht oberste Scheibe ihres Stapels
    if(zst!=sourcestange){
        short neuesZiel=3-zst-sourcestange;//das ist die dritte Stange, weder Ziel noch Source.
        short k=zugaenglich.indexOf(scheibe);
        if(k<=0){
            QMessageBox::warning(0,"Riesenkacke","die scheibe ist die oberste");
            return 0;
        }
        return nextMove(zugaenglich.at(k-1)->getSize(),neuesZiel);
    }else{//Scheibe ist nicht oberste Scheibe, aber auf richtiger Stange
        short k=zugaenglich.indexOf(scheibe);
        return nextMove(zugaenglich.at(k-1)->getSize(),zst);
    }
}

void Solver::stop()
{
    if(animation){
         animation->stop();
        animation->deleteLater();
        Hanoifeld::spielfeld->thinker.reset();
    }
    delete toolbar;
    deleteLater();
}

void Solver::pause()
{
    if(animation->state()==QAbstractAnimation::Running){
        animation->pause();
        pausenAction->setIcon(QIcon(":/images/media-playback-start.png"));
        hanoi->state=Hanoifeld::solverPaused;
    }
    else{
        animation->resume();
        pausenAction->setIcon(QIcon(":/images/media-playback-pause.png"));
        hanoi->state=Hanoifeld::solverRunning;
    }
}

void Solver::next()
{
    animation=nextMove(scheibennr,zielstange);//hierbei startet die Animation schon
    if (animation==nullptr){
        delete toolbar;
        deleteLater();
        return;
    }
    connect(animation->animationAt(1),SIGNAL(finished()),this,SLOT(next()));
    //animation->start(QAbstractAnimation::DeleteWhenStopped);//das ist vmtl. der zweite Start.
}

void Gedankenzeiger::setAnimationGroup(QAnimationGroup *move)
{
    aG=move;
//    aG->connect(aG,&QObject::destroyed,[this](){running=false;});
}

void Gedankenzeiger::start(int del)
{
    running=true;
    delay=del;
    next();
}

void Gedankenzeiger::next()
{
    if(!running) return;
    if(isEmpty()){
        Scheibe::setHighlighted(-1);
        Stange::setHighlighted(-1);
        if(aG!=nullptr)
          aG->start(QAbstractAnimation::DeleteWhenStopped);
        running=false;
        Hanoifeld::spielfeld->repaint();
    }
    else{
        Zug zug=takeFirst();
        Stange::setHighlighted(zug.nach);
        Scheibe::setHighlighted(zug.nr);
        Hanoifeld::spielfeld->repaint();
        if(!Hanoifeld::spielfeld->ssM->isChecked())
          QTimer::singleShot(delay,[this](){next();});
    }
}

void Gedankenzeiger::reset()
{
    running=false;clear();
    Scheibe::setHighlighted(-1);
    Stange::setHighlighted(-1);
}
