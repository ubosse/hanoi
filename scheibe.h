#ifndef SCHEIBE_H
#define SCHEIBE_H
#include <QtCore>
#include <QtWidgets>
class Scheibe;
typedef QList<Scheibe*> Scheibenliste;
class Scheibe : public QWidget
{
    Q_OBJECT
public:
    explicit Scheibe(short asize, QWidget *parent=nullptr);
    static short hoehe;
    short getSize()const{return size;}
    static bool showNumber(){return showNumbers;}
    static void setShowNumber(bool s){showNumbers=s;}
    static void setHighlighted(short s){highlighted=s;}
private:
    static short  highlighted;//diese Scheibe wird rot gezeichnet
    short size;//kleinste Scheibe hat size 0. Sollte dem Index im hanoi.scheiben-Array entsprechen.
    virtual void paintEvent(QPaintEvent *event);
    static bool showNumbers;
signals:

public slots:
};

class Stange:public QWidget
{
    Q_OBJECT
public:
    explicit Stange(short h, QWidget*parent=nullptr);
    Stange():QWidget(){hoehe=0;setGeometry(QRect(0,0,0,0));}
    void setHoehe(short h);
    short getHoehe()const{return hoehe;}
    Scheibenliste scheiben;   
    static void setHighlighted(short s){highlighted=s;}
    short pos;//0 bis 3: wo die Stange steht. (entspricht Zielstange zst im solver.)
private:
    static short  highlighted;//diese Stange wird rot gezeichnet
    virtual void paintEvent(QPaintEvent *event);
    short hoehe;
};
#endif // SCHEIBE_H
