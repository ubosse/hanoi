#include "hanoifeld.h"
#include "solver.h"
//#include "ui_hanoifeld.h"
#include <cstdlib>
#include <ctime>
Hanoifeld*Hanoifeld::spielfeld;
Hanoifeld::Hanoifeld(QWidget *parent) :
    QMainWindow(parent)
  //,ui(new Ui::Hanoifeld)
{
    spielfeld=this;
    //ui->setupUi(this);
    setCentralWidget(new QWidget(this));
    base=new QWidget(centralWidget());
    base->setGeometry(0,0,820,12);
    base->move(30,baseHoehe);
    base->setStyleSheet("background:#700000;");
    base->show();
    for (short i=0;i<4;i++){
        stangen[i].pos=i;
        stangen[i].setHoehe(120);
        stangen[i].setParent(centralWidget());
        stangen[i].move(stangenPos[i]-stangen[i].width()/2,baseHoehe-stangen[i].height()+4);
        if(i<3)
            stangen[i].show();
        else
            stangen[i].hide();
    }
    setGeometry(QRect(pos(),QSize(950,300)));
    toolBar=new QToolBar(this);
    toolBar->addAction(QIcon(":/images/media-seek-backward.png"),"reset",this,SLOT(reset()));
    hoehenSpinner=new QSpinBox(this);
    hoehenSpinner->setMinimum(0);hoehenSpinner->setMaximum(10);hoehenSpinner->setValue(5);
    hoehenSpinner->setToolTip(QString::fromUtf8("Anzahl der Steine"));
    hoehenSpinner->setSuffix(" Scheiben");
    hoehenSpinner->resize(hoehenSpinner->sizeHint());
    connect(hoehenSpinner,SIGNAL(valueChanged(int)),this,SLOT(setHoehe(int)));
    toolBar->addWidget(hoehenSpinner);

    setHoehe(hoehenSpinner->value());
    solveAction=toolBar->addAction(QIcon(":/images/nachdenken.png"),QString::fromUtf8("lösen (L)"),this,SLOT(generalSolve()));
    solveAction->setShortcut(Qt::Key_L);
    /*QMenu*anim=new QMenu(this);
    anim->addAction("saubere Ausgangslage",this,SLOT(testSlot()),Qt::Key_L);
    anim->addAction("beliebige Ausgangslage",this,SLOT(generalSolve()),Qt::Key_G);
    rB->setMenu(anim);
    rB->setPopupMode(QToolButton::InstantPopup);
    toolBar->addWidget(rB);*/
    //toolBar->resize(toolBar->sizeHint());
    centralWidget()->setFocus();
    centralWidget()->setFocusPolicy(Qt::StrongFocus);
    QAction*act=new QAction();
    act->setShortcut(QKeySequence::Quit);
    connect(act,SIGNAL(triggered(bool)),qApp,SLOT(quit()));
    addAction(act);
    QCheckBox*cB=new QCheckBox("Scheibennummern sichtbar");
    connect(cB,SIGNAL(toggled(bool)),this,SLOT(showNumbers(bool)));
    toolBar->addWidget(cB);
    cB=new QCheckBox("beschleunige");
    connect(cB,SIGNAL(toggled(bool)),this,SLOT(setTurbo(bool)));
    toolBar->addWidget(cB);
    toolBar->resize(toolBar->sizeHint());
    toolBar->setMovable(false);
/** jetzt den Gedankenzeiger menu **/
    QMenu*menu=new QMenu();
    //menu->setTitle("Solver");
    QMenu * men=menu->addMenu("Solver");
    showThoughts=men->addAction("sichtbar denken",[this](){thinker.reset();});
    showThoughts->setCheckable(true);
    ssM=men->addAction("single-step-mode");ssM->setCheckable(true);
    ssM->setToolTip(QString::fromUtf8("Die Überlegung, welche Scheiben wohin versetzt werden "
                                      "wird Schritt für Schritt mit Taste N vorgeführt."));
    act=men->addAction("nächster Schritt",[this](){thinker.next();},Qt::Key_N);
    addAction(act);
    //act->setShortcutContext(Qt::ApplicationShortcut);
    toolBar->addWidget(menu);
    addToolBar(toolBar);
    statusBar()->addWidget(zaehler=new QLabel(this));
    zaehler->setText(QString::fromUtf8("Züge: 0"));
    manual=new Handarbeiter(this);
}

Stange *Hanoifeld::getStangeOf(Scheibe *scheibe,short*pos)
{
    return &stangen[getStangenIndexOf(scheibe,pos)];
}

short Hanoifeld::getStangenIndexOf(Scheibe *scheibe,short*pos)
{
    for (short i=0;i<4;i++){
        short n=stangen[i].scheiben.indexOf(scheibe);
        if(n!=-1){
            if (pos!=0) *pos=n;
            return i;
        }
    }
    if(pos!=0) *pos=-1;
    return 3;
}

void Hanoifeld::setHoehe(int h)
{
    hoehe=h;
    //reset();
    for(Scheibe * s:scheiben){
        delete s;
    }
    scheiben.clear();
    clearStangen();
    for(short i=0;i<hoehe;i++){
        Scheibe * s=new Scheibe(i,centralWidget());
        scheiben.append(s);
        stangen[0].scheiben.append(s);
        s->move(stangenPos[0]-s->width()/2,baseHoehe-(h-i)*Scheibe::hoehe);
        s->show();
    }
    setFocus();
}

QSequentialAnimationGroup* Hanoifeld::geheZu(int nr, short nachstange)
{
    static QSequentialAnimationGroup * aG;
    if(nr<0 || nr>=hoehe || nachstange<0 || nachstange>3)
        return nullptr;
    if(movingScheibe==nr){//diese Scheibe bewegt sich bereits.
        aG->stop();
        aG->clear();
        delete aG;
    }
    aG=new QSequentialAnimationGroup(this);
    aG->addAnimation(liftScheibe(nr));
    aG->addAnimation(shiftScheibe(nr,nachstange,500));
    aG->addAnimation(dropScheibe(nr));    
    movingScheibe=nr;
    connect(aG,&QAbstractAnimation::finished,this,[=](){this->movingScheibe=-1;});
    if (!thinker.isEmpty() && showThoughts->isChecked()){
        thinker.setAnimationGroup(aG);
        thinker.start();
    }else{
       aG->start(QAbstractAnimation::DeleteWhenStopped);//hier müsste man ermitteln, ob vorher ein Denkprozess sichtbar gemacht werden soll.
    }
    return aG;
}

QPropertyAnimation *Hanoifeld::liftScheibe(short nr,int duration)
{
    Scheibe*target=scheiben.at(nr);
    QPropertyAnimation*lift=new QPropertyAnimation(target,"geometry",this);
    int yWert=baseHoehe-stangen[0].getHoehe()-10;
    lift->setEndValue(QRect(target->x(),yWert,target->width(),target->height()));
    lift->setDuration(duration);
    return lift;
}

QPropertyAnimation *Hanoifeld::shiftScheibe(short nr, short nachstange,int duration)
{
    Scheibe*target=scheiben.at(nr);
    int yWert=baseHoehe-stangen[0].getHoehe()-10;
    QPropertyAnimation*shift=new QPropertyAnimation(target,"geometry",this);
    shift->setEndValue(QRect(stangenPos[nachstange]-scheiben.at(nr)->width()/2,yWert,target->width(),target->height()));
    shift->setDuration(duration);
    stangen[getStangenIndexOf(target)].scheiben.removeOne(target);
    stangen[nachstange].scheiben.prepend(target);
    return shift;
}

QPropertyAnimation *Hanoifeld::dropScheibe(short nr,int duration)
{
    Scheibe*target=scheiben.at(nr);
    short nachstange=getStangenIndexOf(target);
    //hier fehlt noch der Code, der alle Scheiben plumpsen lässt nach unten.
    QPropertyAnimation*drop=new QPropertyAnimation(target,"geometry",this);
    drop->setEndValue(QRect(stangenPos[nachstange]-scheiben.at(nr)->width()/2,
                baseHoehe-(stangen[nachstange].scheiben.count())*Scheibe::hoehe,target->width(),target->height()));
    drop->setDuration(duration);
    return drop;

}

QList<Zug>* Hanoifeld::createZugliste()
{
    QList<Zug>* liste=new QList<Zug>;
    loese(hoehe,0,2,*liste);
    return liste;
}

void Hanoifeld::loese(short n, short von, short nach, Zugliste &liste)
{
    if(n==1){
        liste.append(Zug(0,von,nach));
        return;
    }
    loese(n-1,von,3-von-nach,liste);
    liste.append(Zug(n-1,von,nach));
    loese(n-1,3-von-nach,nach,liste);
}

void Hanoifeld::keyPressEvent(QKeyEvent *event)
{
    if(manual==nullptr){
       QMainWindow::keyPressEvent(event);
       return;
    }
    manual->KeyboardHandler(event);
}

void Hanoifeld::testSlot()
{
    Scheibe::setShowNumber(false);
    repaint();
    QList<Zug>*liste=createZugliste();
    new Animateur(liste,this);

}

void Hanoifeld::generalSolve()
{
    //Scheibe::setShowNumber(false);
    Solver* solv=new Solver(this,hoehe-1,2);
    solv->next();
}

void Hanoifeld::showNumbers(bool on)
{
    Scheibe::setShowNumber(on);
    setFocus();
    repaint();
}

void Hanoifeld::reset()
{    
    emit resetted();
    QMenu * pop=new QMenu;
    QAction*act1=pop->addAction("alle Scheiben auf linke Stange");
    QAction*act2=pop->addAction("alle Scheiben auf Abstellplatz");
    //QAction*act3=pop->addAction("Scheiben manuell versetzen");
    QAction*act4=pop->addAction("Zufallsverteilung");
    pop->setToolTip(QString::fromUtf8("<p>Jederzeit kann man die Scheiben auch von Hand versetzen."
                                      "Dazu Scheibennummer eingeben, dann Stangennummer (1-4),"
                                      "dann Leertaste zum Fallenlassen.</p>"
                                      "<p>Die Taste T sorgt dafür, dass ein ganzer Turm "
                                      "mit der Scheibe mitversetzt wird.</p>"));
    QAction*choosed=pop->exec(QCursor::pos());
    delete pop;
    if(choosed==nullptr) return;
    if(choosed==act1)
        new Resetter(this,0);
    if(choosed==act2)
        new Resetter(this,3);
    /*if(choosed==act3){
        //if (manual!=nullptr) return;
        Scheibe::setShowNumber(true);
        repaint();
        if(manual==nullptr) manual=new Handarbeiter(this);
        setFocusPolicy(Qt::StrongFocus);
    }*/
    if(choosed==act4){
       new Resetter(this,4);
    }
}

Animateur::Animateur(Zugliste *liste,Hanoifeld*hanoi)
{
    list=liste;
    feld=hanoi;
    connect(hanoi,SIGNAL(resetted()),this,SLOT(stop()));
    toolbar=new QToolBar("Animations-Steuerung");
    pausenAction=toolbar->addAction(QIcon(":/images/media-playback-pause.png"),"Pause",this,SLOT(pause()));
    pausenAction->setShortcut(Qt::Key_Space);
    QAction*act=toolbar->addAction(QIcon(":/images/media-playback-stop.png"),"Stop",this,SLOT(stop()));
    act->setShortcut(Qt::Key_S);
    hanoi->addToolBar(Qt::RightToolBarArea,toolbar);
    //hanoi->insertToolBar(hanoi->toolBar,toolbar);
    nextMove();
}

void Animateur::stop()
{
    list->clear();
    if(animation){
        animation->stop();
        animation->deleteLater();
    }
    delete list;
    delete toolbar;
    deleteLater();
}
void Animateur::pause(){
    if(animation->state()==QAbstractAnimation::Running){
        animation->pause();
        pausenAction->setIcon(QIcon(":/images/media-playback-start.png"));
    }
    else{
        animation->resume();
        pausenAction->setIcon(QIcon(":/images/media-playback-start.png"));
    }
}
void Animateur::nextMove()
{
    if(list->isEmpty()){
        delete list;
        //disconnect(feld,0,this,0);
        delete toolbar;
        deleteLater();
        return;
    }
    Zug z=list->takeFirst();
    animation=feld->geheZu(z.nr,z.nach);
    connect(animation->animationAt(1),SIGNAL(finished()),this,SLOT(nextMove()));
}

Resetter::Resetter(Hanoifeld *hanoi, short dahin)
{
    feld=hanoi;
    feld->clearStangen();
    //das scheibe-array einfach mit 0,...,hoehe-1 befüllen
    for(short j=0;j<feld->getHoehe();j++){
        scheibe.append(j);

    }
    if(dahin==4){//Zufallsverteilung gewünscht. jetzt durchmischeln
        srand(time(NULL));
        for(short j=feld->getHoehe()-1;j>0;j--){
            short zuf=rand()%(j+1);
            short h=scheibe.at(j);
            scheibe[j]=scheibe.at(zuf);
            scheibe[zuf]=h;
        }
    }
    ziel.clear();
    if(dahin<4){
        for(short j=0;j<feld->getHoehe();j++){
            ziel.append(dahin);
        }
    }
    if(dahin==4){//zufällige Verteilung gefordert

        for(short j=0;j<feld->getHoehe();j++){
            ziel.append(rand()%3);
        }
    }
    i=feld->getHoehe()-1;

    next();
}

void Resetter::next()
{
    QSequentialAnimationGroup*aG=feld->geheZu(scheibe.at(i),ziel.at(i));
    if(i==0)
        deleteLater();
    else
    {
        i-=1;
        connect(aG,SIGNAL(finished()),this,SLOT(next()));
    }
}
